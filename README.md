# PasteBinWebCrawler

This project is a web crawler that scans "pastebin.com" every 2 minutes and updates the new pastes on a local DB.

## Getting started

You can run the web crawler app in 2 different ways:
1. Run locally with python:
   1. Clone the repository to your pc
   2. Install the requirements.txt file - "pip3 install -r requirements.txt"
   3. Run web_crawler.py file with python -"python3 -m main.py"
   4. Now, the web crawler runs periodically and stores the new pastes in a local DB called "db.json"

2. Run with docker container:
   1. Clone docker image - "docker push henz2706/pastebin_web_crawler:latest"
   2. Run - "docker run henz2706/pastebin_web_crawler:latest"
   3. Now, the web crawler runs periodically and stores the new pastes in a local DB located in "/app/db.json"

Note: Basic logs stored in "pastebin_web_crawler.log'
