import re
from typing import List

import arrow
from bs4 import BeautifulSoup, Tag

from models.paste_model import PasteModel
from utils import get_nested_html_page_by_url


class PasteBinWebScraper:
    PASTEBIN_URL = 'https://pastebin.com'
    PASTEBIN_ARCHIVE_URL = PASTEBIN_URL + '/archive'
    UNKNOWN_AUTHOR_NAME = ['a guest', 'guest', 'unknown', 'anonymous']
    PASTEBIN_RAW_PASTE_URL = 'https://pastebin.com/raw'

    @staticmethod
    def _get_table_by_html_page(url: str, table_name: str = 'maintable') -> Tag:
        """
        Gets the content from a html page.
        :return:html body element
        """
        table = get_nested_html_page_by_url(url).find("table", attrs={"class": table_name})
        return table.find("tbody")

    @staticmethod
    def _has_the_paste_been_posted_in_the_last_2_minutes(paste_data):
        """
        Checks if the paste has been posted in the last two minutes.
        :return: True if the paste posted in the last two minutes, else False
        """
        pasted_before = paste_data[1].text.strip()
        if re.search(r"min|sec", pasted_before):
            if ("min" in pasted_before and int(pasted_before.split()[0]) < 60) or "sec" in pasted_before:
                return True
        return False

    def get_ids_of_all_pastes(self) -> List[str]:
        """
        Gets the ids of pastes that exists in PasteBin archive page.
        :return: pastes ids
        """
        all_pastes = self._get_table_by_html_page(self.PASTEBIN_ARCHIVE_URL)
        table_rows = all_pastes.find_all("tr")
        recent_pastes_id = []  # list of all pastes_data
        for row in table_rows:
            # paste_data = {}
            paste_data = row.find_all("td")
            if not paste_data:
                # In case of headers.
                continue
            paste_id = paste_data[0].find("a").get("href").replace("/", "")  # Parse the id from the element
            recent_pastes_id.append(paste_id)
        return recent_pastes_id

    def _get_raw_paste_data_by_id(self, paste_id: str) -> str:
        """
        Gets a specific paste raw data by given id
        paste_id : the id of the requested paste
        :return: paste raw data
        """
        pass

    def _get_author_name_from_page(self, nested_html_page: BeautifulSoup) -> str:
        """
        Gets the author name from the paste page.
        """
        author_name = nested_html_page.find('div', attrs={"class": "username"}).text.strip()
        if author_name.lower() in self.UNKNOWN_AUTHOR_NAME:
            return ''
        return author_name

    @staticmethod
    def _get_paste_posted_date(nested_html_page: BeautifulSoup) -> str:
        """
        Gets paste posted date.
        :return: paste posted date
        """
        posted_date_string = nested_html_page.find('div', attrs={"class": "date"}).find('span').get('title')
        posted_date_string = posted_date_string.replace('CDT', 'America/Chicago')  # arrow didn't recognize CDT timezone
        time_format = "dddd D[th] [of] MMMM YYYY hh:mm:ss A ZZZ"
        posted_date = arrow.get(posted_date_string, time_format).utcnow()
        return posted_date.strftime("%Y-%m-%d, %H:%M:%S")

    @staticmethod
    def _get_paste_title(nested_html_page: BeautifulSoup):
        """
        Gets paste's title.
        """
        title = nested_html_page.find('div', attrs={"class": "info-top"}).text.strip()
        if title.lower() == "untitled":
            return ''
        return title

    @staticmethod
    def _get_paste_content(nested_html_page: BeautifulSoup) -> str:
        """
        Gets the paste content
        :return: paste content
        """
        return nested_html_page.find('textarea', attrs={"class": "textarea -raw js-paste-raw"}).text.strip()

    def _get_recent_pastes_data_by_ids(self, pastes_id: List[str]) -> List[PasteModel]:
        """
        Gets a PasteModel list by given list of pastes ids.
        :param pastes_id: pastes ids
        :return: list of PasteModel
        """
        pastes = []
        for paste_id in pastes_id:
            nested_html_page = get_nested_html_page_by_url(self.PASTEBIN_URL + f'/{paste_id}')
            author = self._get_author_name_from_page(nested_html_page)
            date = self._get_paste_posted_date(nested_html_page)
            title = self._get_paste_title(nested_html_page)
            content = self._get_paste_content(nested_html_page)
            pastes.append(PasteModel(id=paste_id, author=author, date=date, title=title, content=content))
        return pastes

    def get_recent_pastes(self, all_pastes_id: List[str]) -> List[PasteModel]:
        """
        Gets data of recent pastes.
        :return: recent pastes
        """
        pastes_data = self._get_recent_pastes_data_by_ids(all_pastes_id)
        return pastes_data
