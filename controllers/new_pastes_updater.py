import logging
import uuid

from dals.new_pastes_dal import NewPastesDAL
from services.pastebin_web_scraper import PasteBinWebScraper
from services.pastes_data_service import PastesDataService

logging.basicConfig(filename='pastebin_web_crawler.log', level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


class NewPastesUpdater:
    def __init__(self):
        self._pastebin_web_scraper: PasteBinWebScraper = PasteBinWebScraper()
        self._pastes_data_service = PastesDataService()
        self._pastes_dal = NewPastesDAL()
        self._logger = logging.getLogger(__name__)

    def scan_for_new_pastes(self):
        """
        Scan for a new pastes and update the db if a new pastes exists.
        """
        scan_id = uuid.uuid4()
        try:
            self._logger.info(f"Starting to scan, Scan id -  {scan_id}")
            all_pastes_ids = self._pastebin_web_scraper.get_ids_of_all_pastes()
            recent_pastes_id = self._pastes_data_service.get_the_recent_pastes_by_pastes_id(all_pastes_ids)
            recent_pastes_data = self._pastebin_web_scraper.get_recent_pastes(recent_pastes_id)
            self._pastes_data_service.insert_new_pastes_to_db(recent_pastes_data, self._logger)
            self._logger.info(f"Scan finished, Scan id - {scan_id}")
        except Exception as ex:
            self._logger.error(f'Scan failed, Scan id - {scan_id}, Error message - {ex}')
