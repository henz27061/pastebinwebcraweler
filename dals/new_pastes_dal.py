from logging import Logger
from typing import List

from tinydb import Query, TinyDB

from models.paste_model import PasteModel

db = TinyDB("db.json")


class NewPastesDAL:
    def __init__(self):
        self._db = db

    def insert_new_pastes_to_db(self, new_pastes: List[PasteModel], logger: Logger):
        """
        Insert the recent posts to the db
        :param new_pastes:List of recent pastes
        :param logger: a python logger
        """
        new_pastes_count = 0
        for paste in new_pastes:
            if self.is_the_paste_exists_in_the_db(paste.id):
                continue
            self._db.insert(paste.dict())
            new_pastes_count += 1
        logger.info(f"{new_pastes_count} new pastes added to the DB!")

    def is_the_paste_exists_in_the_db(self, paste_id: str) -> bool:
        """
        Checks if the paste is already exist in the db.
        :param paste_id: the id of the checked paste
        :return:True if exists else False
        """
        paste_query = Query()
        id_search_result = self._db.search(paste_query.id == paste_id)
        return True if id_search_result else False
