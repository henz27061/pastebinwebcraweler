pydantic~=1.9.1
schedule~=1.1.0
tinydb~=4.7.0
arrow~=1.2.2
beautifulsoup4~=4.8.1
requests~=2.22.0
lxml~=4.9.0