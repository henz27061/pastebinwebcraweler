import requests
from bs4 import BeautifulSoup
from requests import Response


def _send_get_request(url: str) -> Response:
    """
    Gets a response from get request.
    :return: http response
    """
    return requests.get(url)


def get_nested_html_page_by_url(url: str) -> BeautifulSoup:
    """
    Gets a nested html page by given url.
    """
    html_page = _send_get_request(url)
    decoded_html_page = html_page.content.decode("utf-8")
    return BeautifulSoup(decoded_html_page, "lxml")
