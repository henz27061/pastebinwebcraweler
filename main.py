import schedule
from tinydb import TinyDB

from controllers.new_pastes_updater import NewPastesUpdater

db = TinyDB('db.json')

updater = NewPastesUpdater()
updater.scan_for_new_pastes()
schedule.every(2).minutes.do(updater.scan_for_new_pastes)
while True:
    schedule.run_pending()
