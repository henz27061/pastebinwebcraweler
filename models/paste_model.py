from pydantic import BaseModel


class PasteModel(BaseModel):
    author: str
    title: str
    date: str
    content: str
    id: str
