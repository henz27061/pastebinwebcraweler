from logging import Logger
from typing import List

from dals.new_pastes_dal import NewPastesDAL
from models.paste_model import PasteModel


class PastesDataService:
    def __init__(self):
        self.dal = NewPastesDAL()

    def get_the_recent_pastes_by_pastes_id(self, pastes_id: List[str]) -> List[str]:
        """
        Gets a list of the recent pastes' id that don't exist in DB.
        :return: list with the recent pastes id
        """
        recent_pastes_ids = []
        for paste_id in pastes_id:
            if self.dal.is_the_paste_exists_in_the_db(paste_id):
                break
            recent_pastes_ids.append(paste_id)
        return recent_pastes_ids

    def insert_new_pastes_to_db(self, new_pastes: List[PasteModel], logger: Logger):
        """
        Insert the latest pastes into the DB.
        :param new_pastes: list of the recent pastes data (PasteModel)
        :param logger: a python logger
        """
        self.dal.insert_new_pastes_to_db(new_pastes, logger)
